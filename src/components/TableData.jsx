import React from 'react'
import { Container,Table } from 'react-bootstrap'

function TableData(props) {
    let showdata = props.items
    console.log(showdata)
    let finalShow = showdata.filter(show =>{
          return show.amount >0
    })
    return (
<Container>
 <h1>Table</h1>
        <Table striped bordered hover>
        <thead>
            <tr>
            <th>#</th>
            <th>Food</th>
            <th>Amount</th>
            <th>Price</th>
            <th>Total</th>
            </tr>
        </thead>
        <tbody>
            {
            finalShow.map((item,idx)=>
                    <tr key={idx}>
                        <td>{idx+1}</td>
                        <td>{item.title}</td>
                        <td>{item.amount}</td>
                        <td>{item.price}</td>
                        <td>{item.total}</td>
                    </tr>
                )
            }
        </tbody>
        </Table>
</Container>
    )
}

export default TableData
